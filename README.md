# Contents of This File

- Introduction
- Requirements
- Installation
- Configuration

## Introduction

Currently, the autocomplete in Link Field widgets
always shows content suggestions from all content (node) types.

This module adds a Link Field configuration for filtering
the suggested content types in the autocomplete field.

## Requirements

This module requires the following core module:

 * Link

## Installation

 * Install as you would normally install a contributed Drupal module. Visit:
   [Installing Modules](https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules)

## Configuration

 - Simply activate the module, and a series of checkboxes
   (one for each content type) will appear
   in the configuration form of your Link Field field-instances .

 - If you check none, then all content types will appear as suggestions<br>
   in the autocomplete field. Otherwise, only those checked will appear.

 - If you change the field settings later,
   the field will be validated for the new allowed content types.
